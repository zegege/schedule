from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.decorators import action

from .serializers import *
from rest_framework.response import Response


class CourseViewSet(viewsets.ModelViewSet):
    """
    课程管理
    """
    queryset = Course.objects.all()
    serializer_class = LookCourseSerializer
    permission_classes = []
    search_fields = ['name', 'theacher__name']
    filterset_fields = ['is_required', 'theacher']

    def get_permissions(self):
        if self.action in ['create', 'destroy', 'update']:
            return [IsAdminUser()]
        elif self.action in ['my_course']:
            return [IsAuthenticated()]
        return []

    def get_serializer_class(self):
        if self.action in ['create', 'destroy', 'update']:
            return EditCourseSerializer
        return LookCourseSerializer

    def list(self, request, *args, **kwargs):
        user = self.request.user
        queryset = self.filter_queryset(self.get_queryset())
        if user.is_active and not user.is_staff:
            queryset = queryset.exclude(user=user)

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['GET'], detail=False)
    def my_course(self, request):
        user = self.request.user

        queryset = self.filter_queryset(self.get_queryset().filter(user=user))
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)
