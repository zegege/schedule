from rest_framework import routers

from .views import *

router = routers.SimpleRouter()
router.register('course', CourseViewSet, basename='Course')

urlpatterns = router.urls
urlpatterns += [
]
