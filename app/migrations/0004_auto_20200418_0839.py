# Generated by Django 3.0.5 on 2020-04-18 08:39

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_auto_20200418_0829'),
    ]

    operations = [
        migrations.AddField(
            model_name='course',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='course',
            name='start_datetime',
            field=models.DateTimeField(auto_now_add=True, help_text='上课时间'),
        ),
    ]
