from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver


class Course(models.Model):
    name = models.CharField(max_length=99, null=True, blank=True, help_text="课程名称")
    addrerss = models.CharField(max_length=99, null=True, blank=True, help_text="　教室")
    theacher = models.ForeignKey(User, on_delete=models.CASCADE,
                                 max_length=99, null=True, blank=True, help_text="上课老师")
    start_datetime = models.DateTimeField(default=None, null=True, blank=True, help_text="上课时间")
    user = models.ManyToManyField(User, related_name='student', help_text="学生所选课程")
    remak = models.TextField(null=True, default=None, blank=True, help_text="教学要求")
    is_required = models.BooleanField(default=False, help_text="必修课？")
    created_at = models.DateTimeField(auto_now_add=True)
    
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.is_required:
            self.user.add(*User.objects.filter(is_staff=False))
            self.save()
        self.save_base()


class Announcement(models.Model):
    """
    通告
    """
    course = models.ForeignKey(Course, on_delete=models.CASCADE, null=True, blank=True, default=None)
    title = models.CharField(max_length=99, default=None, blank=True, null=True, help_text="通知标题")
    content = models.TextField(default=None, null=True, blank=True, help_text="通知内容")
    created_at = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)


@receiver(post_save, sender=Course)
def create_course(sender, instance, created, **kwargs):
    if created and instance.is_required:
        instance.user.add(*User.objects.filter(is_staff=False))
        instance.save()
