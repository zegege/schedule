from rest_framework import serializers

from accounts.serializers import LookUserSerializer
from .models import *


class EditCourseSerializer(serializers.ModelSerializer):
    user = serializers.CharField(read_only=True, help_text="学生所选课程")
    start_datetime = serializers.DateTimeField(help_text="上课时间", required=False)

    class Meta:
        model = Course
        fields = '__all__'


class LookCourseSerializer(serializers.ModelSerializer):
    theacher = LookUserSerializer()
    user_count = serializers.SerializerMethodField()
    user = serializers.SerializerMethodField()

    start_datetime = serializers.DateTimeField(format='%Y-%m-%d %H:%M')

    class Meta:
        model = Course
        fields = '__all__'

    def get_user(self, obj):
        if self.context['request'].user.is_staff:
            return LookUserSerializer(obj.user.all(), many=True).data
        return []

    def get_user_count(self, obj):
        return obj.user.count()


def id_list(queryset):
    try:
        x = [i.get('id') for i in queryset]
    except Exception:
        x = []
    return x


class AddCourseSerializer(serializers.ModelSerializer):
    change = serializers.ChoiceField(help_text="修改类型", choices=['remove', 'add'])
    course_id = serializers.ChoiceField(choices=id_list(Course.objects.values('id')))

    class Meta:
        model = Course
        fields = ('course_id', 'change')
