from django.contrib.auth.models import User
from rest_framework import serializers


class EditUserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='userprofile.name', required=False)
    sex = serializers.CharField(source='userprofile.sex', required=False)
    birthday = serializers.DateField(source='userprofile.birthday', required=False)
    address = serializers.CharField(source='userprofile.address', required=False)
    phone = serializers.CharField(source='userprofile.phone', required=False)
    username = serializers.CharField(required=False)
    password = serializers.CharField(required=False)

    class Meta:
        model = User
        exclude = ['last_login', 'is_superuser',
                   'first_name', 'last_name', 'is_staff', 'is_active',
                   'groups', 'user_permissions', 'date_joined']

    def create(self, validated_data):
        """
        重写create方法 为了加密密码, 管理员创建的都是老师
        """
        # 调用父类的创建方法
        user = super(EditUserSerializer, self).create(validated_data=validated_data)
        user.set_password(validated_data['password'])
        if self.context.get('request').user.is_superuser:
            user.is_staff = True
            user.userprofile.save()
        user.save()
        return user

    def update(self, instance, validated_data):
        user = super(EditUserSerializer, self).update(instance, validated_data)
        if validated_data.get('password', None):
            user.set_password(validated_data['password'])
        user.save()
        return user


class LookUserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='userprofile.name', required=False)
    sex = serializers.CharField(source='userprofile.sex', required=False)
    birthday = serializers.DateTimeField(source='userprofile.birthday', format='%Y-%m-%d %H:%M', required=False)
    address = serializers.CharField(source='userprofile.address', required=False)
    phone = serializers.CharField(source='userprofile.phone', required=False)
    credit = serializers.IntegerField(source='userprofile.credit')
    date_joined = serializers.DateTimeField(format='%Y-%m-%d %H:%M')
    stu_id = serializers.CharField(source='userprofile.stu_id', read_only=True)

    class Meta:
        model = User
        exclude = ['password', 'last_login', 'first_name', 'last_name', 'groups', 'user_permissions']

