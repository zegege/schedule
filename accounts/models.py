from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserProfile(models.Model):
    stu_id = models.IntegerField(default=0, null=True, blank=True, help_text="学号")
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=99, null=True, blank=True, default=None)
    sex = models.CharField(max_length=16, default="男", null=True, blank=True)
    birthday = models.DateTimeField(default=None, blank=True, null=True, help_text="生日")
    address = models.CharField(max_length=99, null=True, blank=True, default=None)
    phone = models.CharField(max_length=16, null=True, blank=True, default=None)
    credit = models.IntegerField(default=0, help_text="学分", null=True, blank=True)

    class Meta:
        ordering = ['stu_id']


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        stu_id = User.objects.filter(is_staff=False).count()
        UserProfile.objects.create(user=instance, stu_id=stu_id)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.userprofile.save()
