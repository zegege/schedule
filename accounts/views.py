from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from app.models import Course

from .models import *
from .serializers import *


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = LookUserSerializer
    permission_classes = [IsAuthenticated]
    search_fields = ['userprofile__phone', 'userprofile__name']
    filterset_fields = ['userprofile__sex', 'is_staff']

    def get_serializer_class(self):
        if self.action in ['create', 'destroy', 'update']:
            return EditUserSerializer
        elif self.action is 'change_resource':
            from app.serializers import AddCourseSerializer
            return AddCourseSerializer
        elif self.action in ['list', 'retrieve']:
            return LookUserSerializer
        return self.serializer_class

    def get_permissions(self):
        if self.action in ['create', 'destroy', 'retrieve']:
            return [IsAdminUser()]
        return [IsAuthenticated()]

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            errors = serializer.errors
            print(errors)
            for i in errors:
                return Response({
                    'success': False,
                    'msg': errors[i][0]
                }, 400)
        userprofile = False
        if serializer.validated_data.get('userprofile', None):
            userprofile = serializer.validated_data.pop('userprofile')
        user = self.perform_create(serializer)
        if userprofile:
            UserProfile.objects.filter(user=user).update(**userprofile)
        # 直接创建就返回token
        refresh = RefreshToken.for_user(user)
        return Response({
            'success': True,
            'data': {
                'refresh': str(refresh),
                'access': str(refresh.access_token)
            },
            'msg': '成功'
        }, status=201)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        if self.request.user.is_staff:
            instance = self.get_object()
        else:
            instance = self.request.user
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        userprofile_validated_data = serializer.validated_data.pop('userprofile')
        if userprofile_validated_data:
            UserProfile.objects.filter(id=instance.userprofile.id).update(**userprofile_validated_data)
        self.perform_update(serializer)
        user = User.objects.get(id=instance.id)
        data = self.serializer_class(user)
        return Response(data.data, 200)

    @action(methods=['POST'], detail=False)
    def change_course(self, request):
        instance = request.user
        course_id = request.data.get('course_id', None)
        # 增或减
        change = request.data.get('change', None)
        # 需要改变的资源
        resource = instance.student.filter(id=course_id).first()
        try:
            course = Course.objects.get(id=course_id)
        except:
            return Response({
                'success': False,
                'msg': '课程不存在',
            }, status=400)
        if change == 'remove':
            if not resource:
                return Response({
                    'success': False,
                    'msg': '课程不存在',
                }, status=400)
            instance.student.remove(course_id)
        elif change == 'add':
            if resource:
                return Response({
                    'success': False,
                    'msg': '课程已选',
                }, status=200)
            # 同一个时间段选课出现提示
            if instance.student.filter(start_datetime=course.start_datetime):
                return Response({
                    'success': False,
                    'msg': '该时间段已选课程',
                }, status=200)
            instance.student.add(course_id)
        return Response({
            'success': True,
            'msg': '课程更新成功',
        }, status=200)

    def get_queryset(self):
        queryset = self.queryset.filter(is_superuser=False)
        if not self.request.user.is_staff:
            return queryset.filter(id=self.request.user.id)
        return queryset


@api_view(http_method_names=['GET'])
@permission_classes([IsAuthenticated])
def identify(request):
    """
    用于返回当前登录用户的信息
    :param request:
    :return: LookUserSerializer Object
    """
    data = LookUserSerializer(request.user)
    return Response(data.data)
